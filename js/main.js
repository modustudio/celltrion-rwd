
$(function(){
	vid = document.getElementById("mainVideo");
	function videoSize() {
		vid.parentNode.style.width = (vid.offsetHeight * 1.939393939393939)+"px";
	}
	//top banner
	$(window).on("resize", function () {
		if ($('#topBanner').hasClass('off')) {
			$('#topBanner').css({'margin-top': function(){
				var t = ($(window).width() <= 1024) ? '-100px' : '-150px'
				return t;
			}});
		}
		videoSize();
	}).resize();

	$('.bannerBtnClose').on('click', function(){
		if (!$('#topBanner').hasClass('off')) {
			$('#mainWrapper').animate({'margin-top':'0'});
			$('#topBanner').animate({'margin-top': (function(){
				var t = ($(window).width() <= 1024) ? '-100px' : '-150px'
				return t;
			}()) },function(){$(this).addClass('off');videoSize();});
		}
		return false;
	})
	vid.onended = function(e) {
		$('#vedioCtnl').addClass('is-pause').removeClass('is-play');
    }
});

$(window).on("load resize", function () {
    $('#bannerList').touchSlider({
        roll : false,
        resize : true,
		speed : 500,
		autoplay : {
			enable : true,
			pauseHover : true,
			addHoverTarget : $(this).next("#bannerPagination"),
			interval : 7000
		},
        initComplete : function (e) {
            var _this = this;
            var paging = $(this).next("#bannerPagination");
            var item = $(this).find(" > ul > li");

            paging.html("");
            for (var i = 0; i < Math.ceil(($(item).length)/e._view); i++) {
                paging.append('<button type="button">page' + (i+1) + '</button>');
            }
            paging.find("button").bind("click", function (e) {
                _this.go_page($(this).index());
            });
        },
        counter : function (e) {
            $(this).next("#bannerPagination").find("button").removeClass("active").eq(e.current-1).addClass("active");
         }
    });
}).resize();


//visual
$(function(){
	var el = $('#mainVisual');
	var bannerRolling;

	$('#vedioCtnl').on('click',function(e){
		if ($(this).hasClass('is-pause')) {
			vid.play();
			$(this).addClass('is-play').removeClass('is-pause').text('멈춤');
		} else {
			vid.pause();
			$(this).addClass('is-pause').removeClass('is-play').text('재생');
		}
	});

	$(window).on("resize", function () {
		if (navigator.userAgent.match(/Android|Mobile|iP(hone|od|ad)|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/) || ($(window).width() <= 1024)){
			visualStart();
			vid.pause();
			$('#vedioCtnl').addClass('is-pause').removeClass('is-play');
		} else {
			if (!($(window).width() > 1024)) {
				visualStop();
				//vid.load();
			}
		}
	}).resize();

	function visualEffect() {
		$(el).delay(5880).queue(function(){
			$(this).children().eq(1).find('img').attr('src','../../images/main/visual2.gif')
			$(this).css('left','-100%')
    		$(this).dequeue();
		}).delay(5320).queue(function(){
			$(this).children().eq(2).find('img').attr('src','../../images/main/visual3.gif')
			$(this).css('left','-200%')
			$(this).dequeue();
		}).delay(5280).queue(function(){
			$(this).children().eq(0).find('img').attr('src','../../images/main/visual1.gif')
			$(this).css('left','0')
			$(this).dequeue();
		})
	}
	function visualStart() {
		bannerRolling = setInterval(function(){visualEffect()},0);
	}
	function visualStop() {
		clearInterval(bannerRolling);
	}
});
