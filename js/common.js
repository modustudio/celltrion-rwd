$(window).on("load resize", function () {
	$('body').attr('data-responsive',
		(function(){
			var r = ($(window).width() <= 1000) ? true : false
			return r;
		}())
	);
});

//top-bar
$(function(){
	var barBtn = $('#topBarBtn')
	var p = $('#topBar');
	var wrap = $('#mainWrapper');
	if ($(wrap).length) $(p).addClass('open');
	$(barBtn).on('click', function(){
		if ($(p).hasClass('open')) {
			$(p).animate({'margin-top':'-36px'}, 100, function(){$(this).removeClass('open')})
		} else {
			$(p).animate({'margin-top':'0'}, 100, function(){$(this).addClass('open')})
		}
		return false;
	})
});

//accessibleDropDown
$(function() {
	$.fn.accessibleDropDown = function(sub) {
		var el = $(this);
		$(window).resize(function(){
			if ($('body').attr('data-responsive') == 'false'){
				$(el).parents("li").removeClass('open');
			} else {
				$(el).parents("li").removeClass('hover');
			}
		});

		$(el).on("click mouseenter focus touchstart",function(e) {
			if ($('body').attr('data-responsive') == 'false') {
				if (!$(this).parents("li").hasClass("hover")) {
					$(this).parents("li").addClass("hover").siblings("li").removeClass("hover");
				} else {
					$(this).parents("li").removeClass("hover");
				}
				if(e.type == "touchstart") {
					return false;
				}
			} else {
				if(e.type == "click"){
					e.preventDefault();
					if ($(this).parents("li").hasClass("open")) {
						$(this).parents("li").removeClass("open");
					} else {
						$(this).parents("li").siblings().removeClass("open");
						$(this).parents("li").addClass("open");
					}
				}
			}
		});
		$(el).next().on("mouseleave",function() {
			$(this).parents("li").removeClass("hover");
		});
	}
});

$(function(){
	$('.gnb__tit').on('click',function(){
		var gnb = $('.gnb');
		var wrap = $('.wrapper');
		if ($(wrap).hasClass('gnbDimmed')) {
			$(wrap).removeClass('gnbDimmed');
			$(gnb).removeClass('open');
		} else {
			$(wrap).addClass('gnbDimmed');
			$(gnb).addClass('open');
		}
	});
})

/* go-to-top */
$(function(){
	$(window).scroll(function() {
		var scrollPos = $(this).scrollTop();
		var gotopbtn = $("#stickyTopFunc");
		var footHeight = $("#footer").height();
		var docHeight = $(document).height() - $(window).height() - footHeight;

		if (scrollPos < 0) {
			gotopbtn.fadeOut(200);
		} else if (scrollPos >= 0 && scrollPos < docHeight) {
			gotopbtn.removeClass("sticky-go-to-top_fixed");
			gotopbtn.fadeIn(200);
		} else {
			gotopbtn.addClass("sticky-go-to-top_fixed");
			gotopbtn.fadeIn(200);
		}
	});

	// gotop button
	$("#goTop").click(function(e) {
		e.preventDefault();
		jQuery("html,body").scrollTop(0);
	});
});


//테이블 말줄임
function cellNoWrap(target) {
	target.css({
		'display':'inline-block',
		'width':target.parent().width(),
		'white-space':'nowrap',
		'overflow':'hidden',
		'text-overflow':'ellipsis'
	});
}

function cellNormal(target) {
	target.css({'width':'auto','white-space':'normal'});
}

$(document).ready(function(){
	cellNoWrap($('.bbs-link'));
});

$(window).on('resize', function(e) {
	cellNormal($('.bbs-link'))
	setTimeout(function() {
		cellNoWrap($('.bbs-link'));
	}, 250);
});

//input:file
$(document).ready(function() {
  var fileTarget = $('.upload-hidden');

  fileTarget.on('change', function() {
	if (window.FileReader) {
	  // 파일명 추출
	  var filename = $(this)[0].files[0].name;
	} else {
	  // Old IE 파일명 추출
	  var filename = $(this).val().split('/').pop().split('\\').pop();
	};

	$(this).parent().siblings('.upload-name').val(filename);
  });

  //preview image
  var imgTarget = $('.upload-hidden');

  imgTarget.on('change', function() {
	var parent = $(this).parent().parent();
	parent.children('.upload-display').remove().end().removeClass('upload-set-v1_thumb');

	if (window.FileReader) {
	  //image 파일만
	  if (!$(this)[0].files[0].type.match(/image\//)) return;

	  var reader = new FileReader();
	  reader.onload = function(e) {
		var src = e.target.result;
		parent.prepend('<span class="upload-display"><img src="' + src + '" class="upload-thumb"></span>').addClass('upload-set-v1_thumb');
	  }
	  reader.readAsDataURL($(this)[0].files[0]);
	} else {
	  //$(this)[0].select();
	  //$(this)[0].blur();
	  //var imgSrc = document.selection.createRange().text;
	  //parent.prepend('<span class="upload-display"><img class="upload-thumb"></span>').addClass('upload-set-v1_thumb');

	  //var img = $(this).parent().siblings('.upload-display').find('img');
	  //img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\"" + imgSrc + "\")";
	}
  });

});

//새소식

$(document).ready(function(){
	listN = $('.mini-notice__list li').length;
	$(window).load(function(){
		if(listN < 4){$('.mini-notice__list li').clone().appendTo('.mini-notice__list');}
	})
	bannerH = $('.mini-notice__list li').height();
	if(listN > 1){
		$('.mini-notice__list').css({'top':-(bannerH)*2});
		i = 0
		$('.mini-notice__btn_next').on('click',function(){
			$('.mini-notice__list').animate({'top':-(bannerH)},'slow',function(){
				$('.mini-notice__list li').last().prependTo('.mini-notice__list');
				$('.mini-notice__list').css({'top':-(bannerH)*2});
			});
			clearInterval(time1);
			timeView();
		});
		$('.mini-notice__btn_prev').on('click',function(){
			$('.mini-notice__list').animate({'top':-(bannerH*3)},'slow',function(){
				$('.mini-notice__list').css({'top':-(bannerH)*2});
				$('.mini-notice__list li').first().appendTo('.mini-notice__list');
			});
			clearInterval(time1);
			timeView();
		});
		function timeView(){
		time1 =	setInterval(function() {
				$('.mini-notice__list').animate({'top':-(bannerH*3)},'slow',function(){
					$('.mini-notice__list').css({'top':-(bannerH)*2});
					$('.mini-notice__list li').first().appendTo('.mini-notice__list');
				});
			}, 3000);
		}
		timeView();
	}else{return;}
});
