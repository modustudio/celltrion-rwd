var gulp = require('gulp'),
//var sass = require('gulp-sass');
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber'),
    connect = require('gulp-connect'),
    includer = require('gulp-html-ssi'),
    //imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    hologram = require('gulp-hologram'),
    kss = require('gulp-kss');


gulp.task('serve', function() {
	connect.server({
		root: './',
		port: 8000,
		livereload: true,
        open: {
			browser: 'chrome' // chrome(안되면 Google Chrome), iexplore, ...
		}
	});
});

gulp.task('kss', function() {
    gulp.src('./scss/**/*')
    .pipe(kss({
        overview: './styleguide.md',
        templateDirectory: './styleguide-template'
    }))
    .pipe(gulp.dest('./styleguide/'))
    .pipe(connect.reload())
});
gulp.task('compass', function() {
	gulp.src('./scss/**/*.{scss,sass}')
	.pipe( plumber({
		errorHandler: function (error) {
			console.log(error.message);
			this.emit('end');
		}
	}) )
	.pipe(compass({
		css: './css',
		sass: './scss',
		style: 'compact' // nested, expaned, compact, compressed
	}))
	.pipe( gulp.dest('./css') )
    .pipe(connect.reload())
});

gulp.task('htmlSSI', function() {
	gulp.src('./shtml/**/*.html')
		.pipe(includer())
		.pipe(gulp.dest('./html'))
        .pipe(connect.reload())
});

gulp.task('imgmin', function() {
	gulp.src(['./imgmin/before/**/*'])
		//.pipe(imagemin({
		//	progressive: true,
        //    interlaced:true,
		//	use: [pngquant()]
		//}))
		.pipe(gulp.dest('./imgmin/after/'));
});

//Watch task
gulp.task('watch',function() {
    gulp.watch('./scss/**/*.{scss,sass}',['compass','kss']);
    gulp.watch('./shtml/**/*.html',['htmlSSI']);
    //gulp.watch('./imgmin/before/**/*',['imgmin']);
});

gulp.task('default', ['serve','compass','htmlSSI','watch','kss']);
